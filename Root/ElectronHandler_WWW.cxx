
// electron specific includes
#include "CxAODMaker_WZ/ElectronHandler_WWW.h"
#include "CxAODMaker/EventInfoHandler.h"
#include "CxAODTools_WWW/CommonProperties_WWW.h"
#include "TSystem.h"

ElectronHandler_WWW::ElectronHandler_WWW(const std::string& name, ConfigStore& config, xAOD::TEvent* event,
                                         EventInfoHandler& eventInfoHandler)
    : ElectronHandler(name, config, event, eventInfoHandler),
      m_trigToolMediumLHIsoFixedCutLoose(nullptr),
      m_trigEffToolMediumLHIsoFixedCutLoose(nullptr),
      m_effToolIsoFixedCutLooseTightLH_PLV(nullptr) {
  using std::placeholders::_1;
  m_selectFcns.clear();
  m_selectFcns.push_back(std::bind(&ElectronHandler_WWW::passVetoElectron, this, _1));
  m_selectFcns.push_back(std::bind(&ElectronHandler_WWW::passZElectron, this, _1));
  m_selectFcns.push_back(std::bind(&ElectronHandler_WWW::passWElectron, this, _1));
}

ElectronHandler_WWW::~ElectronHandler_WWW() {
  delete m_trigToolMediumLHIsoFixedCutLoose;
  delete m_trigEffToolMediumLHIsoFixedCutLoose;
  delete m_effToolIsoFixedCutLooseTightLH_PLV;
}

EL::StatusCode ElectronHandler_WWW::initializeTools() {
  ElectronHandler::initializeTools();

  // JL: initialize truth classifier object
  electronTruthClassifier = new RegionClass("ElectronTruthClassifier");

  // TODO atlfast / fullsim
  bool isFullSim = !m_eventInfoHandler.get_isAFII();
  //  enum DataType {Data = 0, Full = 1, FastShower = 2, Fast = 3, True = 4}
  int dataType = PATCore::ParticleDataType::Full;
  if (!isFullSim) dataType = PATCore::ParticleDataType::Fast;

  // Add Trigger tool for medium LH

  m_trigToolMediumLHIsoFixedCutLoose =
      new AsgElectronEfficiencyCorrectionTool("AsgElectronEfficiencyCorrectionTool_trigMediumLHIsoFixedCutLoose");
  m_trigToolMediumLHIsoFixedCutLoose->msg().setLevel(m_msgLevel);
  std::vector<std::string> correctionFileNameListTrigMediumLHIsoFixedCutLoose;
  correctionFileNameListTrigMediumLHIsoFixedCutLoose.push_back(
      "ElectronEfficiencyCorrection/2015_2017/rel21.2/Consolidation_September2018_v1/trigger/"
      "efficiencySF.SINGLE_E_2015_e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose_2016_2018_e26_lhtight_nod0_ivarloose_OR_e60_"
      "lhmedium_nod0_OR_e140_lhloose_nod0.MediumLLH_d0z0_v13_isolFCLoose.root");
  TOOL_CHECK("ElectronHandler::initializeTools()",
             m_trigToolMediumLHIsoFixedCutLoose->setProperty("CorrectionFileNameList", correctionFileNameListTrigMediumLHIsoFixedCutLoose));
  TOOL_CHECK("ElectronHandler::initializeTools()", m_trigToolMediumLHIsoFixedCutLoose->setProperty("ForceDataType", dataType));
  TOOL_CHECK("ElectronHandler::initializeTools()", m_trigToolMediumLHIsoFixedCutLoose->setProperty("CorrelationModel", "TOTAL"));
  TOOL_CHECK("ElectronHandler::initializeTools()", m_trigToolMediumLHIsoFixedCutLoose->initialize());

  m_trigEffToolMediumLHIsoFixedCutLoose =
      new AsgElectronEfficiencyCorrectionTool("AsgElectronEfficiencyCorrectionTool_trigEffMediumLHIsoFixedCutLoose");
  m_trigEffToolMediumLHIsoFixedCutLoose->msg().setLevel(m_msgLevel);
  std::vector<std::string> correctionFileNameListTrigEffMediumLHIsoFixedCutLoose;
  correctionFileNameListTrigEffMediumLHIsoFixedCutLoose.push_back(
      "ElectronEfficiencyCorrection/2015_2017/rel21.2/Consolidation_September2018_v1/trigger/"
      "efficiency.SINGLE_E_2015_e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose_2016_2018_e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_"
      "nod0_OR_e140_lhloose_nod0.MediumLLH_d0z0_v13_isolFCLoose.root");
  TOOL_CHECK("ElectronHandler::initializeTools()", m_trigEffToolMediumLHIsoFixedCutLoose->setProperty(
                                                       "CorrectionFileNameList", correctionFileNameListTrigEffMediumLHIsoFixedCutLoose));
  TOOL_CHECK("ElectronHandler::initializeTools()", m_trigEffToolMediumLHIsoFixedCutLoose->setProperty("ForceDataType", dataType));
  TOOL_CHECK("ElectronHandler::initializeTools()", m_trigEffToolMediumLHIsoFixedCutLoose->setProperty("CorrelationModel", "TOTAL"));
  TOOL_CHECK("ElectronHandler::initializeTools()", m_trigEffToolMediumLHIsoFixedCutLoose->initialize());

  // efficiency tool for PLV based on FixedCutLoose Iso TightLH
  //-----------------
  std::string custom_dir = gSystem->Getenv("WorkDir_DIR");
  custom_dir += "/data/CxAODOperations_WWW/";
  std::string custom_file = "efficiencySF.Isolation.TightLLH_d0z0_v13_PLVeto_CFTtight_ambiguity0_isolFixedCutLoose.root";

  m_effToolIsoFixedCutLooseTightLH_PLV =
      new AsgElectronEfficiencyCorrectionTool("AsgElectronEfficiencyCorrectionTool_IsoFixedCutLooseTightLH_PLV");
  m_effToolIsoFixedCutLooseTightLH_PLV->msg().setLevel(m_msgLevel);
  std::vector<std::string> correctionFileNameListIsoFixedCutLooseTightLH_PLV;
  correctionFileNameListIsoFixedCutLooseTightLH_PLV.push_back(custom_dir + custom_file);
  TOOL_CHECK("ElectronHandler::initializeTools()", m_effToolIsoFixedCutLooseTightLH_PLV->setProperty(
                                                       "CorrectionFileNameList", correctionFileNameListIsoFixedCutLooseTightLH_PLV));
  TOOL_CHECK("ElectronHandler::initializeTools()", m_effToolIsoFixedCutLooseTightLH_PLV->setProperty("ForceDataType", dataType));
  TOOL_CHECK("ElectronHandler::initializeTools()", m_effToolIsoFixedCutLooseTightLH_PLV->setProperty("CorrelationModel", "TOTAL"));
  TOOL_CHECK("ElectronHandler::initializeTools()", m_effToolIsoFixedCutLooseTightLH_PLV->initialize());

  m_sysToolList.push_back(m_trigToolMediumLHIsoFixedCutLoose);
  m_sysToolList.push_back(m_trigEffToolMediumLHIsoFixedCutLoose);
  m_sysToolList.push_back(m_effToolIsoFixedCutLooseTightLH_PLV);

  return EL::StatusCode::SUCCESS;
}

EL::StatusCode ElectronHandler_WWW::decorateOriginParticle(const xAOD::Electron* electron) {
  ElectronHandler::decorateOriginParticle(electron);

  // JL: IFF truth variables testing
  int isPrompt = -1;
  int isFake = -1;
  int truthStatus = -1;
  int isMC = m_eventInfoHandler.get_isMC();

  if (isMC) {
    try {
      isPrompt = electronTruthClassifier->isPrompt(electron);
      isFake = electronTruthClassifier->isFake(electron);
      truthStatus = electronTruthClassifier->getClass(electron);
    } catch (...) {
      Warning("ElectronHandler_WWW::decorate()", "electronTruthClassifer does not work! Set IFF truth variables to -1.");
    }
  }
  Props::isPrompt.set(electron, isPrompt);
  Props::isFake.set(electron, isFake);
  Props::truthStatus.set(electron, truthStatus);

  // All values here are independent of energy calibration
  const xAOD::TrackParticle* trackPart = electron->trackParticle();
  // For NoBLayer Hit Electrons
  // reference: https://twiki.cern.ch/twiki/bin/view/AtlasProtected/EGammaIdentificationRun2#Electron_and_photon_identificati
  // https://gitlab.cern.ch/atlas/athena/blob/21.2/PhysicsAnalysis/ElectronPhotonID/ElectronPhotonSelectorTools/Root/TElectronLikelihoodTool.cxx#L179
  // Total number of cuts: 12
  // Pass NBlayer: 4th Cut (bit 3)
  int isMedium_NoBLayerLH = 0;
  int isTight_NoBLayerLH = 0;
  unsigned int value = 0;
  if (trackPart) {
    value = electron->auxdata<unsigned int>("DFCommonElectronsLHMediumIsEMValue");
    isMedium_NoBLayerLH = static_cast<int>(value == (1 << 3));
    if (isMedium_NoBLayerLH) {
      value = electron->auxdata<unsigned int>("DFCommonElectronsLHTightIsEMValue");
      isTight_NoBLayerLH = static_cast<int>(value == (1 << 3));
    }
  }
  Props::isMedium_NoBLayerLH.set(electron, isMedium_NoBLayerLH);
  Props::isTight_NoBLayerLH.set(electron, isTight_NoBLayerLH);

  return EL::StatusCode::SUCCESS;
}

EL::StatusCode ElectronHandler_WWW::decorate(xAOD::Electron* electron) {
  int isMediumLH = Props::isMediumLH.get(electron);
  int isTightLH = Props::isTightLH.get(electron);
  int isMC = m_eventInfoHandler.get_isMC();

  double trigEFFmediumLHIsoFixedCutLoose = 1.;
  double trigSFmediumLHIsoFixedCutLoose = 1.;
  double effSFIsoFixedCutLooseTightLH_PLV = 1.;

  const xAOD::CaloCluster* cluster = electron->caloCluster();
  float cluster_eta = 10;
  if (cluster) {
    //cluster_eta = cluster->eta();
    cluster_eta = cluster->etaBE(2);
  }

  //NM 16-01-19: egamma recommendations now start at 7GeV, so scale factor should be there... to check
  if (isMC && electron->pt() >= 7000. &&
      fabs(cluster_eta) < 2.47) {  //TODO: Check again this safety guard after full-recommendations are out!
    if (isMediumLH) {
      if (m_trigToolMediumLHIsoFixedCutLoose->getEfficiencyScaleFactor(*electron, trigSFmediumLHIsoFixedCutLoose) ==
          CP::CorrectionCode::Error) {
        Error("ElectronHandler::decorate()", "ElectronEfficiencyCorrection returned CP::CorrectionCode::Error");
        return EL::StatusCode::FAILURE;
      }
      if (m_trigEffToolMediumLHIsoFixedCutLoose->getEfficiencyScaleFactor(*electron, trigEFFmediumLHIsoFixedCutLoose) ==
          CP::CorrectionCode::Error) {
        Error("ElectronHandler::decorate()", "ElectronEfficiencyCorrection returned CP::CorrectionCode::Error");
        return EL::StatusCode::FAILURE;
      }
      if (isTightLH) {
        if (m_effToolIsoFixedCutLooseTightLH_PLV->getEfficiencyScaleFactor(*electron, effSFIsoFixedCutLooseTightLH_PLV) ==
            CP::CorrectionCode::Error) {
          Error("ElectronHandler::decorate()", "ElectronEfficiencyCorrection returned CP::CorrectionCode::Error");
          return EL::StatusCode::FAILURE;
        }
      }
    }
  }

  Props::trigEFFmediumLHIsoFixedCutLoose.set(electron, trigEFFmediumLHIsoFixedCutLoose);
  Props::trigSFmediumLHIsoFixedCutLoose.set(electron, trigSFmediumLHIsoFixedCutLoose);
  Props::effSFIsoFixedCutLooseTightLH_PLV.set(electron, effSFIsoFixedCutLooseTightLH_PLV);

  // ECIDS variables
  // Electron Charge ID Selector Tool (Charge Flip killer)
  // https://twiki.cern.ch/twiki/bin/viewauth/AtlasProtected/ElectronChargeFlipTaggerTool
  int ECIDS = static_cast<int>(electron->auxdata<char>("DFCommonElectronsECIDS"));
  float ECIDSResult = static_cast<float>(electron->auxdata<double>("DFCommonElectronsECIDSResult"));
  Props::ECIDS.set(electron, ECIDS);
  Props::ECIDSResult.set(electron, ECIDSResult);

  // Electron addAmbiguity
  int addAmbiguity = electron->auxdata<int>("DFCommonAddAmbiguity");
  Props::addAmbiguity.set(electron, addAmbiguity);

  // Electron Author
  Props::Author.set(electron, electron->author());

  // truth information for electrons
  int truthType = 0;
  int truthOrigin = 0;
  int firstEgMotherPdgId = 0;
  if (isMC) {
    truthType = (electron)->auxdata<int>("truthType");
    truthOrigin = (electron)->auxdata<int>("truthOrigin");
    firstEgMotherPdgId = (electron)->auxdata<int>("firstEgMotherPdgId");
  }
  Props::truthType.set(electron, truthType);
  Props::truthOrigin.set(electron, truthOrigin);
  Props::firstEgMotherPdgId.set(electron, firstEgMotherPdgId);

  return ElectronHandler::decorate(electron);
}

EL::StatusCode ElectronHandler_WWW::calibrateCopies(xAOD::ElectronContainer* particles, const CP::SystematicSet& sysSet) {
  //calibration tool
  //-----------------
  // tell tool to apply systematic variation

  CP_CHECK("ElectronHandler_WWW::calibrateCopies()", m_trigToolMediumLHIsoFixedCutLoose->applySystematicVariation(sysSet), m_debug);
  CP_CHECK("ElectronHandler_WWW::calibrateCopies()", m_trigEffToolMediumLHIsoFixedCutLoose->applySystematicVariation(sysSet), m_debug);
  CP_CHECK("ElectronHandler_WWW::calibrateCopies()", m_effToolIsoFixedCutLooseTightLH_PLV->applySystematicVariation(sysSet), m_debug);

  return ElectronHandler::calibrateCopies(particles, sysSet);
}

bool ElectronHandler_WWW::passVetoElectron(xAOD::Electron* electron) {
  bool passSel = true;
  // set priority of first cut to some value, which should be different for
  // other selection functions
  // cut names have to be unique (no check currently!)
  // if (passSel) m_cutflow->count("VetoElectronInput", 100);

  if (!(electron->isGoodOQ(xAOD::EgammaParameters::BADCLUSELECTRON))) passSel = false;
  if (passSel) m_cutflow->count("isGoodOQElectron");

  // egamma recommendation is 4.5GeV
  if (!(electron->pt() > 5000.)) passSel = false;
  if (passSel) m_cutflow->count("pt>5GeV");
  Props::passPreSel.set(electron, passSel);

  if (electron->caloCluster()) {
    if (!(fabs(electron->caloCluster()->etaBE(2)) < 2.47)) passSel = false;
  } else {
    if (!(fabs(electron->eta()) < 2.5)) passSel = false;
    Warning("ElectronHandler_WZ::passVetoElectron",
            "Did not find caloCluster, use eta() instead of caloCluster()->etaBE(2) to check eta range!");
  }
  if (passSel) m_cutflow->count("|eta|<2.5");

//if (!(Props::isLooseNoBLLH.get(electron))) passSel = false;
  //if (passSel) m_cutflow->count("isLooseNoBLLH", 100);

  if (!(fabs(Props::d0sigBL.get(electron)) < 5)) passSel = false;
  if (passSel) m_cutflow->count("|d0sigBL|<5");

  if (!(fabs(Props::z0sinTheta.get(electron)) < 0.5)) passSel = false;
  if (passSel) m_cutflow->count("|z0sinTheta|<0.5");

  if (!(Props::isLooseLH.get(electron))) passSel = false;
  if (passSel) m_cutflow->count("VetoElectronSelected");
  Props::isVetoElectron.set(electron, passSel);

  return passSel;
}

bool ElectronHandler_WWW::passZElectron(xAOD::Electron* electron) {
  // For both ZElectron and NoBLElectron
  bool passSel = true;
    
  if (!(Props::isVetoElectron.get(electron))) passSel = false;


//  if (!Props::passPreSel.get(electron)) passSel = false;
//  if (passSel) m_cutflow->count("ZElectronInput", 200);

  if (!(electron->pt() > 15000.)) passSel = false;
  if (passSel) m_cutflow->count("pt>15GeV");

  if (electron->caloCluster()) {
    if (fabs(electron->caloCluster()->etaBE(2)) < 1.52 && fabs(electron->caloCluster()->etaBE(2)) > 1.37) passSel = false;
  } else {
//    if (fabs(electron->eta()) < 1.52 && fabs(electron->eta()) > 1.37) passSel = false;
    Warning("ElectronHandler_WWW::passZElectron",
            "Did not find caloCluster, use eta() instead of caloCluster()->etaBE(2) to check eta range!");
  }
  if (passSel) m_cutflow->count("ExcludeCrackRegion");



  bool passLoose = passSel;
  bool passNoBL = passSel;

  if (!(Props::isMediumLH.get(electron))) passLoose = false;
  if (passLoose) m_cutflow->count("ZElectronSelected");
  Props::isLooseElectron.set(electron, passLoose);

//  if (!(Props::isTight_NoBLayerLH.get(electron))) passNoBL = false;
//  if (passNoBL) m_cutflow->count("NoBLElectronSelected");
  Props::isNoBLElectron.set(electron, passNoBL);

  passSel = passLoose || passNoBL;

  //if (passSel) m_cutflow->count("METRebuild");
  Props::forMETRebuild.set(electron, passSel);

  return passSel;
}

bool ElectronHandler_WWW::passWElectron(xAOD::Electron* electron) {
  bool passSel = true;

  if (!(Props::isLooseElectron.get(electron))) passSel = false;

  if (!(electron->pt() > 20000.)) passSel = false;
  if (passSel) m_cutflow->count("pt>20GeV");

 
  // if (passSel) m_cutflow->count("WElectronInput", 300);

  if (!(Props::isTightLH.get(electron))) passSel = false;
  if (passSel) m_cutflow->count("isTightLH", 300);

  Props::isSignalElectron.set(electron, passSel);
  if (passSel) m_cutflow->count("WElectronSelected");

  return passSel;
}

EL::StatusCode ElectronHandler_WWW::writeCustomVariables(xAOD::Electron* inElectron, xAOD::Electron* outElectron, bool isKinVar,
                                                         bool isWeightVar, const TString& sysName) {
  // JL: testing; may need to put this somewhere else, I am not really sure how systematics are handled here
  if (m_eventInfoHandler.get_isMC() && !isKinVar && !isWeightVar) {
    Props::isPrompt.copy(inElectron, outElectron);
    Props::isFake.copy(inElectron, outElectron);
    Props::truthStatus.copy(inElectron, outElectron);
  }

  if (!isKinVar && !isWeightVar) {
    Props::Author.copy(inElectron, outElectron);
    Props::truthOrigin.copy(inElectron, outElectron);
    Props::truthType.copy(inElectron, outElectron);
    Props::firstEgMotherPdgId.copy(inElectron, outElectron);
    Props::addAmbiguity.copy(inElectron, outElectron);
    Props::ECIDS.copy(inElectron, outElectron);
    Props::ECIDSResult.copy(inElectron, outElectron);
    Props::isMedium_NoBLayerLH.copy(inElectron, outElectron);
    Props::isTight_NoBLayerLH.copy(inElectron, outElectron);
    // Trigger efficiencies are not affected by any variation (opposed to trigger SFs)
    Props::trigEFFmediumLHIsoFixedCutLoose.copy(inElectron, outElectron);
    // Used for PLVTight/Loose Working Point
    if (inElectron->isAvailable<float>("ptvarcone30_TightTTVALooseCone_pt1000")) {
      outElectron->setIsolationValue(inElectron->isolationValue(xAOD::Iso::ptvarcone30_TightTTVALooseCone_pt1000),
                                     xAOD::Iso::ptvarcone30_TightTTVALooseCone_pt1000);
    }
    Props::isVetoElectron.copy(inElectron, outElectron);
    Props::isLooseElectron.copy(inElectron, outElectron);
    Props::forMETRebuild.copy(inElectron, outElectron);
    Props::isNoBLElectron.copy(inElectron, outElectron);
    Props::isSignalElectron.copy(inElectron, outElectron);
  }

  // Write variables only for specific variation. Has to be written to Nominal as well.
  bool isTrigVar = (sysName == "Nominal" || sysName.BeginsWith("EL_EFF_Trigger_"));
  // bool isIDVar   = (sysName == "Nominal" || sysName.BeginsWith("EL_EFF_ID_"));
  // bool isRecoVar = (sysName == "Nominal" || sysName.BeginsWith("EL_EFF_Reco_"));
  bool isIsoVar = (sysName == "Nominal" || sysName.BeginsWith("EL_EFF_Iso_"));

  if (!isKinVar) {
    if (m_eventInfoHandler.get_isMC() && isTrigVar) {
      Props::trigSFmediumLHIsoFixedCutLoose.copy(inElectron, outElectron);
    }
    if (m_eventInfoHandler.get_isMC() && isIsoVar) {
      Props::effSFIsoFixedCutLooseTightLH_PLV.copy(inElectron, outElectron);
    }
  }

  return EL::StatusCode::SUCCESS;
}
