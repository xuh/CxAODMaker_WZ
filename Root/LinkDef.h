#include "CxAODMaker_WZ/AnalysisBase_WWW.h"

#ifdef __CINT__

#pragma link off all globals;
#pragma link off all classes;
#pragma link off all functions;

#pragma link C++ nestedclass;
#pragma link C++ class AnalysisBase_WWW+;

#endif
