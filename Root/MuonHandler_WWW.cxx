#include <iostream>

//muon specific includes
#include "CxAODMaker_WZ/MuonHandler_WWW.h"
// The order cannot change
#include "CxAODMaker/EventInfoHandler.h"

MuonHandler_WWW::MuonHandler_WWW(const std::string& name, ConfigStore& config, xAOD::TEvent* event, EventInfoHandler& eventInfoHandler)
    : MuonHandler(name, config, event, eventInfoHandler), m_muonEfficiencyScaleFactorsFCLoose_FixedRadIso_PLV(nullptr) {
  using std::placeholders::_1;
  m_selectFcns.clear();
  m_selectFcns.push_back(std::bind(&MuonHandler_WWW::passVetoMuon, this, _1));
  m_selectFcns.push_back(std::bind(&MuonHandler_WWW::passZMuon, this, _1));
  m_selectFcns.push_back(std::bind(&MuonHandler_WWW::passWMuon, this, _1));
}

MuonHandler_WWW::~MuonHandler_WWW() { delete m_muonEfficiencyScaleFactorsFCLoose_FixedRadIso_PLV; }

EL::StatusCode MuonHandler_WWW::initializeTools() {
  MuonHandler::initializeTools();

  // initialize truth classifier object
  muonTruthClassifier = new RegionClass("MuonTruthClassifier");

  // SF for PLV
  // Set SF File Name and Path
  std::string custom_dir = gSystem->Getenv("WorkDir_DIR");
  custom_dir += "/data/CxAODOperations_WWW/";
  std::string custom_file = "Iso_mptLeptonVetoIso_Z.root";

  m_muonEfficiencyScaleFactorsFCLoose_FixedRadIso_PLV = new CP::MuonEfficiencyScaleFactors("MuonEfficiencyToolFCLoose_FixedRadIso_PLV");
  m_muonEfficiencyScaleFactorsFCLoose_FixedRadIso_PLV->msg().setLevel(m_msgLevel);
  TOOL_CHECK("MuonHandler::initializeTools()",
             m_muonEfficiencyScaleFactorsFCLoose_FixedRadIso_PLV->setProperty("WorkingPoint", "PromptLeptonIso"));
  TOOL_CHECK("MuonHandler::initializeTools()",
             m_muonEfficiencyScaleFactorsFCLoose_FixedRadIso_PLV->setProperty("CustomInputFolder", custom_dir));
  TOOL_CHECK("MuonHandler::initializeTools()",
             m_muonEfficiencyScaleFactorsFCLoose_FixedRadIso_PLV->setProperty("CustomFileCombined", custom_file));
  TOOL_CHECK("MuonHandler::initializeTools()", m_muonEfficiencyScaleFactorsFCLoose_FixedRadIso_PLV->initialize());

  m_sysToolList.push_back(m_muonEfficiencyScaleFactorsFCLoose_FixedRadIso_PLV);

  return EL::StatusCode::SUCCESS;
}

EL::StatusCode MuonHandler_WWW::decorateOriginParticle(const xAOD::Muon* muon) {
  MuonHandler::decorateOriginParticle(muon);

  // JL: IFF truth variables testing
  int isPrompt = -1;
  int isFake = -1;
  int truthStatus = -1;
  int isMC = m_eventInfoHandler.get_isMC();

  if (isMC) {
    try {
      isPrompt = muonTruthClassifier->isPrompt(muon);
      isFake = muonTruthClassifier->isFake(muon);
      truthStatus = muonTruthClassifier->getClass(muon);
    } catch (...) {
      Warning("MuonHandler_WWW::decorate()", "muonTruthClassifer does not work! Set IFF truth variables to -1.");
    }
  }

  Props::isPrompt.set(muon, isPrompt);
  Props::isFake.set(muon, isFake);
  Props::truthStatus.set(muon, truthStatus);

  return EL::StatusCode::SUCCESS;
}

EL::StatusCode MuonHandler_WWW::decorate(xAOD::Muon* muon) {
  bool accepted = m_muonSelectionToolLoose->accept(*muon);

  int isMC = m_eventInfoHandler.get_isMC();
  float fCLoose_FixedRadIso_PLVSF = 1.;

  if (isMC) {
    if (accepted) {
      CP_CHECK("MuonHandler_WWW::decorate()",
               m_muonEfficiencyScaleFactorsFCLoose_FixedRadIso_PLV->getEfficiencyScaleFactor(*muon, fCLoose_FixedRadIso_PLVSF), m_debug);
    }
  }

  Props::fCLoose_FixedRadIso_PLVSF.set(muon, fCLoose_FixedRadIso_PLVSF);

  // truth information for muons
  int truthType = 0;
  int truthOrigin = 0;
  if (isMC) {
    truthType = (muon)->auxdata<int>("truthType");
    truthOrigin = (muon)->auxdata<int>("truthOrigin");
  }
  Props::truthType.set(muon, truthType);
  Props::truthOrigin.set(muon, truthOrigin);

  return MuonHandler::decorate(muon);  // this is strange: why call this?
}

EL::StatusCode MuonHandler_WWW::calibrateCopies(xAOD::MuonContainer* particles, const CP::SystematicSet& sysSet) {
  // tell tool to apply systematic variation
  CP_CHECK("MuonHandler::calibrateCopies()", m_muonEfficiencyScaleFactorsFCLoose_FixedRadIso_PLV->applySystematicVariation(sysSet),
           m_debug);

  return MuonHandler::calibrateCopies(particles, sysSet);
}

bool MuonHandler_WWW::passVetoMuon(xAOD::Muon* muon) {
  bool passSel = true;
  //if (passSel) m_cutflow->count("VetoMuonInput", 100);

  //apply selection for combined or segment tagged muons for now
//  if (!(Props::acceptedMuonTool.get(muon))) passSel = false;
//  if (passSel) m_cutflow->count("IsAtLeastVetoMuon", 100);  //This is the starting quality we require in the tool

  if (!(muon->pt() > 5000.)) passSel = false;
  if (passSel) m_cutflow->count("pt>5GeV");

  if (!(fabs(muon->eta()) < 2.7)) passSel = false;
  if (passSel) m_cutflow->count("|eta|<2.7");

  if (!(Props::muonQuality.get(muon) < 3)) passSel = false;  // 0 == tight, 1 == medium, 2 == Loose, 3 == VeryLoose
  if (passSel) m_cutflow->count("LooseMuonQuality");

  if (!((fabs(Props::z0sinTheta.get(muon)) < 0.5) && (fabs(muon->eta()) < 2.5))) passSel = false;
  if (passSel) m_cutflow->count("|z0sinTheta|<0.5");

  if (!(fabs(Props::d0sigBL.get(muon)) < 3)) passSel = false;
  if (passSel) m_cutflow->count("|d0sigBL|<3");

  if (passSel) m_cutflow->count("VetoMuonSelected");
  Props::isVetoMuon.set(muon, passSel);
  Props::passPreSel.set(muon, passSel);

  return passSel;
}

bool MuonHandler_WWW::passZMuon(xAOD::Muon* muon) {
  bool passSel = true;
  if (!(Props::isVetoMuon.get(muon))) passSel = false;
  //if (passSel) m_cutflow->count("LooseMuonInput", 200);
  // if (!(muon->pt() > 7000.)) passSel = false;
  // if (passSel) m_cutflow->count("pt>7GeV", 200);
 
  if (!(muon->pt() > 15000.)) passSel = false;
  if (passSel) m_cutflow->count("pt>15GeV");

  if (!(Props::muonQuality.get(muon) < 2)) passSel = false;  // 0 == tight, 1 == medium. Selects medium & tight muons
  if (passSel) m_cutflow->count("MediumMuonQuality");

  if (!(fabs(muon->eta()) < 2.5)) passSel = false;
  if (passSel) m_cutflow->count("|eta|<2.5");

  if (passSel) m_cutflow->count("Z_MuonSelected");
  Props::isLooseMuon.set(muon, passSel);
  Props::forMETRebuild.set(muon, passSel);

  return passSel;
}

bool MuonHandler_WWW::passWMuon(xAOD::Muon* muon) {
  bool passSel = true;

  //check loose selection is passed
  if (!(Props::isLooseMuon.get(muon))) passSel = false;
  // if (passSel) m_cutflow->count("SignalMuonInput", 300);
  
  if (!(muon->pt() > 20000.)) passSel = false;
  if (passSel) m_cutflow->count("pt>20GeV");
   
  if (!(Props::muonQuality.get(muon) < 1)) passSel = false;  // 0 == tight, 1 == medium. Selects  tight muons
  if (passSel) m_cutflow->count("TightMuonQuality");


  // Should be included in acceptedMuonTool
  // if (!(Props::passedIDCuts.get(muon))) passSel = false;
  // if (passSel) m_cutflow->count("SignalMuon IDCuts");

  if (passSel) m_cutflow->count("SignalMuonSelected");
  Props::isSignalMuon.set(muon, passSel);

  return passSel;
}

EL::StatusCode MuonHandler_WWW::writeCustomVariables(xAOD::Muon* inMuon, xAOD::Muon* outMuon, bool isKinVar, bool isWeightVar,
                                                     const TString& sysName) {
  // bool isEffVar  = (sysName == "Nominal" || sysName.BeginsWith("MUON_EFF_"));
  // bool isTTVAVar = (sysName == "Nominal" || sysName.BeginsWith("MUON_TTVA_"));
  bool isIsoVar = (sysName == "Nominal" || sysName.BeginsWith("MUON_ISO_"));  // this seems odd

  // JL: testing; may need to put this somewhere else, I am not really sure how systematics are handled here
  if (m_eventInfoHandler.get_isMC() && !isWeightVar && !isKinVar) {
    Props::isPrompt.copy(inMuon, outMuon);
    Props::isFake.copy(inMuon, outMuon);
    Props::truthStatus.copy(inMuon, outMuon);
  }

  // those are independent of kinematic systematics
  if (!isKinVar) {
    if (m_eventInfoHandler.get_isMC() && isIsoVar) {
      Props::fCLoose_FixedRadIso_PLVSF.copy(inMuon, outMuon);
    }
  }

  // those are independent of weight systematics
  if (!isWeightVar && !isKinVar) {
    Props::isVetoMuon.copy(inMuon, outMuon);
    Props::isLooseMuon.copy(inMuon, outMuon);
    Props::isSignalMuon.copy(inMuon, outMuon);
    Props::muonQuality.copy(inMuon, outMuon);
    Props::truthOrigin.copy(inMuon, outMuon);
    Props::truthType.copy(inMuon, outMuon);
  }

  return EL::StatusCode::SUCCESS;
}
