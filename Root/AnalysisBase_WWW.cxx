#include "EventLoop/Job.h"
#include "EventLoop/OutputStream.h"
#include "EventLoop/StatusCode.h"
#include "EventLoop/Worker.h"

#include "CxAODMaker_WZ/AnalysisBase_WWW.h"

#include "CxAODMaker_WZ/ElectronHandler_WWW.h"
#include "CxAODMaker_WZ/ForwardElectronHandler_WWW.h"
#include "CxAODMaker_WZ/JetHandler_WWW.h"
#include "CxAODMaker_WZ/MuonHandler_WWW.h"
// #include "CxAODMaker/PhotonHandler.h"
#include "CxAODMaker/TauHandler.h"
// #include "CxAODMaker/DiTauJetHandler.h"
// #include "CxAODMaker/FatJetHandler.h"
// #include "CxAODMaker/TrackJetHandler.h"
#include "CxAODMaker/EventInfoHandler.h"
#include "CxAODMaker/EventSelector.h"
#include "CxAODMaker/METHandler.h"
#include "CxAODMaker/TruthJetHandler.h"
// #include "CxAODMaker/JetRegression.h"
// #include "CxAODMaker/JetSemileptonic.h"
#include "CxAODMaker/TruthEventHandler.h"

#include "CxAODMaker/TruthProcessor.h"

#include "CxAODTools_WWW/WWW2lepEvtSelection.h"
// #include "CxAODTools_WWW/WWW1lepEvtSelection.h"

#include "SampleHandler/DiskOutputLocal.h"
#include "SampleHandler/DiskOutputXRD.h"

#include <fstream>
#include "CxAODTools/OverlapRemoval.h"
#include "TSystem.h"

// this is needed to distribute the algorithm to the workers
ClassImp(AnalysisBase_WWW)  //; - comment required for proper auto-formatting

    EL::StatusCode AnalysisBase_WWW::setupJob(EL::Job& job) {
  // Here you put code that sets up the job on the submission object
  // so that it is ready to work with your algorithm, e.g. you can
  // request the D3PDReader service or add output files.  Any code you
  // put here could instead also go into the submission script.  The
  // sole advantage of putting it here is that it gets automatically
  // activated/deactivated when you add/remove the algorithm from your
  // job, which may or may not be of value to you.

  Info("setupJob()", "Setting up job.");

  // check if ConfigStore is set
  if (!m_config) {
    Error("setupJob()", "ConfigStore not setup! Remember to set it via setConfig()!");
    return EL::StatusCode::FAILURE;
  }

  Info("setupJob()", "maxEvents = %d", m_config->get<int>("maxEvents"));

  job.useXAOD();

  // let's initialize the algorithm to use the xAODRootAccess package
  xAOD::Init("AnalysisBase").ignore();  // call before opening first file

  // tell EventLoop about our output xAOD:
  EL::OutputStream out("CxAOD", "xAODNoMeta");
  std::string m_PATH = "none";
  m_config->getif<std::string>("outputFolderUser", m_PATH);
  std::string driver = "direct";
  m_config->getif<std::string>("driver", driver);
  std::string sample_in = "";
  m_config->getif<std::string>("sample_in", sample_in);
  bool grid = sample_in.find("grid") != std::string::npos;

  if (!grid && driver == "condor" && m_PATH != "none") {
    if (m_PATH.find("root") == 0)
      out.output(new SH::DiskOutputXRD(m_PATH));
    else
      out.output(new SH::DiskOutputLocal(m_PATH));
  }

  out.options()->setString(EL::OutputStream::optMergeCmd, "xAODMerge -m LumiMetaDataTool");
  job.outputAdd(out);

  //std::cout << "DEBUG delete old analysis context." << std::endl;
  //delete m_analysisContext;
  //std::cout << "DEBUG delete new analysis context." << std::endl;

  m_config->getif<bool>("applyJetRegression", m_applyJetRegression);
  bool tmp_doTraining;
  m_config->getif<bool>("JetRegression.doTraining", tmp_doTraining);

  // hardcode output stream for JetRegression
  if (m_applyJetRegression && tmp_doTraining) {
    EL::OutputStream outreg("RegTrees");
    job.outputAdd(outreg);
  }

  m_config->getif<bool>("applyMETAfterSelection", m_applyMETAfterSelection);

  m_skipNoPV = true;  // default to true
  m_config->getif<bool>("skipNoPV", m_skipNoPV);

  return EL::StatusCode::SUCCESS;
}

EL::StatusCode AnalysisBase_WWW::initializeSampleInfo() {
  std::cout << "Inside initializeSampleInfo" << std::endl;

  std::ifstream file;
  std::string sampleInfo_File = gSystem->Getenv("WorkDir_DIR");
  sampleInfo_File += "/data/CxAODOperations_WWW/DxAOD/info/sample_info.txt";

  std::cout << sampleInfo_File << std::endl;

  file.open(sampleInfo_File.c_str());

  while (!file.eof()) {
    // read line
    std::string lineString;
    getline(file, lineString);
    // std::cout << lineString << std::endl;

    // skip empty lines
    // TODO - is there a better way to implement this check?
    // if (lineString.find(".") > 1000) {
    //   continue;
    // }

    // skip lines starting with #
    if (lineString.find("#") == 0) {
      continue;
    }

    // store in map
    std::stringstream line(lineString);
    int dsid;
    std::string shortname;
    std::string longname;
    std::string nominal;
    std::string veto;

    line >> dsid >> shortname >> longname >> nominal >> veto;

    if (m_NominalDSIDs.count(dsid) != 0) {
      Warning("initializeSampleInfo()", "Skipping duplicated mc_channel_number for line '%s'.", lineString.c_str());
      continue;
    }
    m_NominalDSIDs[dsid] = (nominal == "nominal");
  }
  file.close();
  return EL::StatusCode::SUCCESS;
}


EL::StatusCode AnalysisBase_WWW::initializeHandlers() {
  Info("AnalysisBase_WWW::initializeHandlers()", "Initialize handlers.");


  // initialize EventInfoHandler
  m_eventInfoHandler = new EventInfoHandler(*m_config, m_event);
  //pass information if running on derivation and MC / data to EventInfoHandler in order to have it available in the other object handlers
  m_eventInfoHandler->set_isMC(m_isMC);
  m_eventInfoHandler->set_isDerivation(m_isDerivation);
  m_eventInfoHandler->set_derivationName(m_derivationName);
  m_eventInfoHandler->set_isAFII(m_isAFII);
  m_eventInfoHandler->set_pTag(m_pTag);
  m_eventInfoHandler->set_hasNoFJDetectorEta(m_hasNoFJDetectorEta);
  m_eventInfoHandler->set_mcChanNr(m_mcChanNr);

  // check pile up reweight Files contain MC dataset before initializing PU reweight in EventInfoHandler
  if (m_isMC) EL_CHECK("AnalysisBase_WWW::initializeHandlers()", checkPileupRwFiles());

  EL_CHECK("AnalysisBase_WWW::initializeHandlers()", m_eventInfoHandler->initialize());

  std::string selectionName = "";
  m_config->getif<std::string>("selectionName", selectionName);

  // note: the names of the handlers determine which collections are
  //       read, see also CxAODMaker-job.cfg

  // these have global pointer to be used e.g. in event selection
  if (m_isMC) {
    m_truthProcessor = new TruthProcessor("truthProcessor", *m_config, m_event, *m_eventInfoHandler);
    m_truthEventHandler = registerHandler<TruthEventHandler>("truthEvent");
    m_truthjetHandler = registerHandler<TruthJetHandler>("truthJet");
    // m_truthfatjetHandler      = registerHandler<TruthJetHandler>("truthfatJet");
    // m_truthWZjetHandler    = registerHandler<TruthJetHandler>("truthWZJet");
    // m_truthWZfatjetHandler    = registerHandler<TruthJetHandler>("truthWZfatJet");
  }
  m_muonHandler = registerHandler<MuonHandler_WWW>("muon");
  m_tauHandler = registerHandler<TauHandler>("tau");
  // m_ditauHandler      = registerHandler<DiTauJetHandler>("diTauJet");
  m_electronHandler = registerHandler<ElectronHandler_WWW>("electron");
  m_forwardElectronHandler = registerHandler<ForwardElectronHandler_WWW>("forwardElectron");
  // m_photonHandler   = registerHandler<PhotonHandler>("photon");
  m_jetHandler = registerHandler<JetHandler_WWW>("jet");
  // m_fatjetHandler   = registerHandler<FatJetHandler>("fatJet");
  // m_fatjetAltHandler   = registerHandler<FatJetHandler>("fatJetAlt");
  // m_trackjetHandler = registerHandler<TrackJetHandler>("trackJet");
  // m_vrtrackjetHandler = registerHandler<TrackJetHandler>("vrtrackJet");
  // if(m_event->contains<xAOD::JetContainer>("AntiKt10LCTopoTrimmedPtFrac5SmallR20ExCoM2SubJets")) m_comtrackjetHandler = registerHandler<TrackJetHandler>("comtrackJet");
  m_METHandler = registerHandler<METHandler>("MET");
  // m_METHandlerMJTight = registerHandler<METHandler>("METMJTight");
  // m_METHandlerFJVT = registerHandler<METHandler>("METFJVT");
  // m_METHandlerMJMUTight = registerHandler<METHandler>("METMJMUTight");
  // m_METHandlerMJMiddle = registerHandler<METHandler>("METMJMiddle");
  // m_METHandlerMJLoose = registerHandler<METHandler>("METMJLoose");

  // these are spectators: they are calibrated and written to output,
  //                       but are not used in the event selection

  // registerHandler<JetHandler>("jetSpectator");
  m_METTrackHandler = registerHandler<METHandler>("METTrack");
  if (m_isMC) {
    registerHandler<METHandler>("METTruth");
  }

  // note: the names of the handlers determine which collections are
  //       read, see also framework-run.cfg

  // alternative: manual handler initialization (e.g. with different constructor)
  //  std::string containerName;
  //  std::string name = "muon";
  //  m_config->getif<std::string>(name + "Container", containerName);
  //  if ( ! containerName.empty() ) {
  //    m_muonHandler = new MuonHandler(name, *m_config, m_event, *m_eventInfoHandler);
  //    m_objectHandler.push_back( m_muonHandler );
  //  }

  // for tau truth matching
  if (m_tauHandler) {
    // changed this to use the TruthProcessor
    m_tauHandler->setTruthProcessor(m_truthProcessor);
  }

  bool m_isEmbedding = false;
  if (!m_isMC && !m_isEmbedding) {
    TString sampleName = wk()->metaData()->castString("sample_name");
    if (sampleName.Contains("Egamma")) m_eventInfoHandler->set_TriggerStream(1);
    if (sampleName.Contains("Muons")) m_eventInfoHandler->set_TriggerStream(2);
    if (sampleName.Contains("Jet")) m_eventInfoHandler->set_TriggerStream(3);
  }

  return EL::StatusCode::SUCCESS;
}

EL::StatusCode AnalysisBase_WWW::initializeSelection() {
  Info("initializeSelection()", "...");

  // determine selection name

  std::string selectionName = "";
  bool autoDetermineSelection;
  m_config->getif<bool>("autoDetermineSelection", autoDetermineSelection);
  if (!autoDetermineSelection) {
    m_config->getif<std::string>("selectionName", selectionName);
  } else {
    // TString sampleName = wk()->metaData()->castString("sample_name");
    // if      (sampleName.Contains("HIGG5D1")) selectionName = "0lep";
    // else if (sampleName.Contains("HIGG5D2")) selectionName = "1lep";
    // else if (sampleName.Contains("HIGG2D4")) selectionName = "2lep";
    // else if (sampleName.Contains("HIGG5D3")) selectionName = "vbf";
    // //else if (sampleName.Contains("EXOT3")) selectionName = "vgamma";
    // else {
    Error("initialize()", "Could not auto determine selection!");
    return EL::StatusCode::FAILURE;
    // }
  }

  // initialize event selection

  //new VGammaEvtSelection();

  bool applySelection = false;
  m_config->getif<bool>("applyEventSelection", applySelection);
  if (applySelection) {
    Info("initialize()", "Applying selection: %s", selectionName.c_str());
    if (selectionName == "2lep") m_selector->setSelection(new WWW2lepEvtSelection());
    // else if (selectionName == "1lep") m_selector->setSelection(new WWW1lepEvtSelection());
    else {
      Error("initialize()", "Unknown selection requested!");
      return EL::StatusCode::FAILURE;
    }
  }

  // initialize overlap removal (possibly analysis specific)

  OverlapRemoval* overlapRemoval = new OverlapRemoval(*m_config);
  EL_CHECK("initializeSelection()", overlapRemoval->initialize());
  m_selector->setOverlapRemoval(overlapRemoval);

  if (applySelection && (!m_muonHandler || !m_electronHandler || !m_jetHandler || !m_METHandler)) {
    Error("initialize()", "Not all collections for event selection are defined!");
    return EL::StatusCode::FAILURE;
  }

  return EL::StatusCode::SUCCESS;
}
