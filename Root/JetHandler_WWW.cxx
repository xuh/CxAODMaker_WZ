#include <iostream>

// jet specific includes
#include "CxAODMaker_WZ/JetHandler_WWW.h"

JetHandler_WWW::JetHandler_WWW(const std::string& name, ConfigStore& config, xAOD::TEvent* event, EventInfoHandler& eventInfoHandler)
    : JetHandler(name, config, event, eventInfoHandler),
      m_bTagTool(0)

{
  using std::placeholders::_1;
  m_selectFcns.clear();
  m_selectFcns.push_back(std::bind(&JetHandler_WWW::passSignalJet, this, _1));
}

JetHandler_WWW::~JetHandler_WWW() { delete m_bTagTool; }

EL::StatusCode JetHandler_WWW::initializeTools() {
  EL_CHECK("JetHandler::initializeTools()", JetHandler::initializeTools());

  // BTaggingSelectionTool
  // https://twiki.cern.ch/twiki/bin/view/AtlasProtected/BTagTaggerRecommendationsRelease21#201810_and_201903_series_tagger
  // https://twiki.cern.ch/twiki/bin/view/AtlasProtected/BTaggingBenchmarksRelease21#AntiKt4EMPFlowJets
  m_bTagTool = new BTaggingSelectionTool("BTaggingSelectionTool");
  m_bTagTool->msg().setLevel(m_msgLevel);
  TOOL_CHECK("JetHandler_WWW()::initializeTools()", m_bTagTool->setProperty("MaxEta", 2.5));
  TOOL_CHECK("JetHandler_WWW()::initializeTools()", m_bTagTool->setProperty("MinPt", 20000.));
  TOOL_CHECK("JetHandler_WWW()::initializeTools()", m_bTagTool->setProperty("JetAuthor", "AntiKt4EMPFlowJets"));
  TOOL_CHECK("JetHandler_WWW()::initializeTools()", m_bTagTool->setProperty("TaggerName", "DL1r"));
  TOOL_CHECK(
      "JetHandler_WWW()::initializeTools()",
      m_bTagTool->setProperty("FlvTagCutDefinitionsFileName", "xAODBTaggingEfficiency/13TeV/2017-21-13TeV-MC16-CDI-2019-07-30_v1.root"));
  TOOL_CHECK("JetHandler_WWW()::initializeTools()", m_bTagTool->setProperty("OperatingPoint", "FixedCutBEff_85"));
  TOOL_CHECK("JetHandler_WWW()::initializeTools()", m_bTagTool->initialize());

  return EL::StatusCode::SUCCESS;
}

bool JetHandler_WWW::passSignalJet(xAOD::Jet* jet) {
  bool passSel = true;
  if (passSel) m_cutflow->count("jet input", 100);

  // Jet pT and eta cut
  if (!(jet->pt() > 20000.)) passSel = false;
  if (passSel) m_cutflow->count("jet pt>20GeV");
  if (!(fabs(jet->eta()) < 4.5)) passSel = false;
  if (passSel) m_cutflow->count("jet |eta|<4.5");
  if ((fabs(jet->eta()) >= 2.5) && !(jet->pt() > 30000.)) passSel = false;
  if (passSel) m_cutflow->count("forward jet pt>30GeV");

  // JVT: https://twiki.cern.ch/twiki/bin/view/AtlasProtected/JVTCalibrationRel21
  if (!(Props::PassJvtMedium.get(jet))) passSel = false;
  if (passSel) m_cutflow->count("JVT");

  // ok, I'll destroy the nice cutflow, but it's to fix a bug on badJets not being passed to OR
  //if (!(Props::goodJet.get(jet))) passSel = false;
  //if (passSel) m_cutflow->count("GoodJet");
  // passPreSel must be set BEFORE the goodJet requirement
  Props::passPreSel.set(jet, passSel);

  if (!(Props::goodJet.get(jet))) passSel = false;
  if (passSel) m_cutflow->count("GoodJet");

  // btag for central jets
  int isTagged = 0;
  double tagWeight = -1;
  if (passSel) {
    if (m_bTagTool->accept(*jet)) isTagged = 1;
    m_bTagTool->getTaggerWeight(*jet, tagWeight);
  }
  BTagProps::tagWeight.set(jet, tagWeight);
  BTagProps::isTagged.set(jet, isTagged);
  Props::isBTag.set(jet, isTagged);    // Used for overlap removel
  Props::isVetoJet.set(jet, passSel);  // Used for overlap removel

  if (passSel) m_cutflow->count("SignalJet selected");

  Props::isSignalJet.set(jet, passSel);

  return passSel;
}

EL::StatusCode JetHandler_WWW::writeCustomVariables(xAOD::Jet* inJet, xAOD::Jet* outJet, bool /*isKinVar*/, bool isWeightVar,
                                                    const TString& /*sysName*/) {
  if (!isWeightVar) {
    Props::isVetoJet.copy(inJet, outJet);
    Props::isSignalJet.copy(inJet, outJet);
    BTagProps::tagWeight.copy(inJet, outJet);
    BTagProps::isTagged.copy(inJet, outJet);
    Props::isBTag.copy(inJet, outJet);
  }

  return EL::StatusCode::SUCCESS;
}
