#include <iostream>

#include "CxAODMaker_WZ/ForwardElectronHandler_WWW.h"
#include "CxAODTools_WWW/CommonProperties_WWW.h"

ForwardElectronHandler_WWW::ForwardElectronHandler_WWW(const std::string& name, ConfigStore& config, xAOD::TEvent* event,
                                                       EventInfoHandler& eventInfoHandler)
    : ForwardElectronHandler(name, config, event, eventInfoHandler) {
  using std::placeholders::_1;
  m_selectFcns.clear();
  m_selectFcns.push_back(std::bind(&ForwardElectronHandler_WWW::passVetoElectron, this, _1));
}

ForwardElectronHandler_WWW::~ForwardElectronHandler_WWW() {
  //delete tools
}

bool ForwardElectronHandler_WWW::passVetoElectron(xAOD::Electron* electron) {
  bool passSel = true;
  // set priority of first cut to some value, which should be different for
  // other selection functions
  // cut names have to be unique (no check currently!)
  // if (passSel) m_cutflow->count("VetoElectronInput", 100);

  if (!(Props::isLooseLH.get(electron))) passSel = false;
  if (passSel) m_cutflow->count("isLooseLH", 100);

  // egamma recommendation is 4.5GeV
  if (!(electron->pt() > 4500.)) passSel = false;
  if (passSel) m_cutflow->count("pt>4.5GeV");

  if (passSel) m_cutflow->count("VetoElectronSelected");

  // forward electrons are only used for veto
  Props::isVetoElectron.set(electron, passSel);
  Props::passPreSel.set(electron, passSel);

  // forward electrons are not used as loose/tight electrons in the analysis
  Props::isLooseElectron.set(electron, false);
  Props::isNoBLElectron.set(electron, false);
  Props::isSignalElectron.set(electron, false);

  return passSel;
}

EL::StatusCode ForwardElectronHandler_WWW::writeCustomVariables(xAOD::Electron* inElectron, xAOD::Electron* outElectron, bool, bool,
                                                                const TString&) {
  // This method is meant to be overridden in derived handlers for writing
  // additional decorations. Argument names are omitted here to avoid warnings.
  Props::isVetoElectron.copy(inElectron, outElectron);
  Props::isLooseElectron.copy(inElectron, outElectron);
  Props::isNoBLElectron.copy(inElectron, outElectron);
  Props::isSignalElectron.copy(inElectron, outElectron);

  return EL::StatusCode::SUCCESS;
}
