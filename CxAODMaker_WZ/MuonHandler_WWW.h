// Dear emacs, this is -*-c++-*-

#ifndef CxAODMaker_MuonHandler_WWW_H
#define CxAODMaker_MuonHandler_WWW_H

// used to get IFF truth classification
#include "RegionClass/RegionClass.h"

#include "AsgTools/AnaToolHandle.h"
#include "CxAODMaker/MuonHandler.h"
#include "CxAODTools_WWW/CommonProperties_WWW.h"
#include "MuonEfficiencyCorrections/MuonEfficiencyScaleFactors.h"
#include "MuonSelectorTools/MuonSelectionTool.h"
#include "TSystem.h"

class MuonHandler_WWW : public MuonHandler {
 public:
  MuonHandler_WWW(const std::string& name, ConfigStore& config, xAOD::TEvent* event, EventInfoHandler& eventInfoHandler);

  virtual ~MuonHandler_WWW();

  virtual EL::StatusCode initializeTools() override;

 protected:
  // JL: declare truth classifier object
  RegionClass* muonTruthClassifier;

  CP::MuonEfficiencyScaleFactors* m_muonEfficiencyScaleFactorsFCLoose_FixedRadIso_PLV;

  // selection functions
  bool passVetoMuon(xAOD::Muon* part);
  bool passZMuon(xAOD::Muon* part);  // override the same function in CxAODMaker
  bool passWMuon(xAOD::Muon* part);

  //Override function (calls base)
  virtual EL::StatusCode decorateOriginParticle(const xAOD::Muon* muon) override;
  virtual EL::StatusCode decorate(xAOD::Muon* muon) override;
  virtual EL::StatusCode calibrateCopies(xAOD::MuonContainer* particles, const CP::SystematicSet& sysSet) override;
  virtual EL::StatusCode writeCustomVariables(xAOD::Muon* inMuon, xAOD::Muon* outMuon, bool isKinVar, bool isWeightVar,
                                              const TString& sysName) override;
};

#endif
